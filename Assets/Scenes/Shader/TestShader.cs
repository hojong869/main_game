using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShader : MonoBehaviour
{
    public Material shadowMaterial;
    public float shadowThreshold;

    //float curTime, coolTime = 1f;
    // Start is called before the first frame update
    void Start()
    {
        //���� ��鰪
        shadowThreshold = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        //��� �� ����
        /*
        curTime += Time.deltaTime;
        if(curTime > coolTime)
        {
            if (shadowThreshold < 0.56f) shadowThreshold += 0.001f;
        }
        */

    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        shadowMaterial.SetFloat("_ShadowThreshold", shadowThreshold);
        Graphics.Blit(source, destination, shadowMaterial);
    }
}
