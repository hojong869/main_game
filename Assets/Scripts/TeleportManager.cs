using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportManager : MonoBehaviour
{
    GameObject gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Player")
        {
            GameObject.Find("Center").GetComponent<ViewManager>().rotationX = 0;
            GameObject.Find("Center").GetComponent<ViewManager>().rotationY = 0;
            if ((gameManager.GetComponent<MapManager>().totalLoop % 2) == 0) gameManager.GetComponent<MapManager>().MovePlayer("MapSub01");
            else gameManager.GetComponent<MapManager>().MovePlayer("MapSub02");
            gameManager.GetComponent<MapManager>().totalLoop++;
            //gameManager.GetComponent<MapManager>().MovePlayer(transform.parent.name);
        }
    }
}
