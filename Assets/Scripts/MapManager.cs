using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    GameObject player;
    List<GameObject> maps = new List<GameObject>();

    public int totalLoop = 1, triggerCount, hintCount;
    public float time, lastTime;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        maps.Add(GameObject.Find("MapSub01"));
        maps.Add(GameObject.Find("MapSub02"));
    }

    // Update is called once per frame
    void Update()
    {
        time = Time.time;
    }

    public void MovePlayer(string mapName)
    {
        for(int i=0; i< maps.Count; i++)
        {
            if (mapName == maps[i].name) player.transform.position = maps[i].transform.FindChild("SpawnPoint").position;
        }
    }

    public void TotalLoopUp()
    {
        lastTime = time;
        totalLoop++;
        time = 0;
    }

    public void TriggerCountUp()
    {
        triggerCount++;
    }
    public void HintCountUp()
    {
        hintCount++;
    }
}
