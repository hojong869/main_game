using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    GameObject[] views = new GameObject[4];
    int isViewType;

    // Start is called before the first frame update
    void Start()
    {
        views[0] = GameObject.Find("Center");
        views[1] = GameObject.Find("Forward");
        views[2] = GameObject.Find("BackTop");
        views[3] = GameObject.Find("Top");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            isViewType++;
            if (isViewType > 3) isViewType = 0;
        }
        IsCameraMove(isViewType);
    }

    public void IsCameraMove(int type)
    {
        switch (type)
        {
            case 0:
                transform.position = views[0].transform.position;
                transform.rotation = views[0].transform.rotation;
                break;
            case 1:
                transform.position = views[1].transform.position;
                transform.rotation = views[1].transform.rotation;
                break;
            case 2:
                transform.position = views[2].transform.position;
                transform.rotation = views[2].transform.rotation;
                break;
            case 3:
                transform.position = views[3].transform.position;
                transform.rotation = views[3].transform.rotation;
                break;
            default:
                transform.position = views[0].transform.position;
                transform.rotation = views[0].transform.rotation;
                break;

        }
    }
}
