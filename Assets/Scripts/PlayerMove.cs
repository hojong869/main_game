using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float speed;
    bool isJump;

    GameObject mainView;
    float rotationX;

    // Start is called before the first frame update
    void Start()
    {
        mainView = GameObject.Find("Center");
    }

    // Update is called once per frame
    void Update()
    {
        PlayerRotate();
        if (Input.GetKey(KeyCode.W)) transform.Translate(Vector3.forward * speed * Time.deltaTime);
        if (Input.GetKey(KeyCode.S)) transform.Translate(Vector3.back * (speed/2) * Time.deltaTime);
        if (Input.GetKey(KeyCode.D)) transform.Translate(Vector3.right * speed * Time.deltaTime);
        if (Input.GetKey(KeyCode.A)) transform.Translate(Vector3.left * speed * Time.deltaTime);
    }

    public void PlayerRotate()
    {
        rotationX = mainView.GetComponent<ViewManager>().rotationX;
        transform.eulerAngles = new Vector3(0, rotationX, 0.0f);
    }
}
