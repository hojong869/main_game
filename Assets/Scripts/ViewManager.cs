using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewManager : MonoBehaviour
{
    public float sensitivity = 300f;
    public float rotationX = 0.0f, rotationY = 0.0f;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        
        player = GameObject.Find("Player");
        GameObject.Find("GrapPoint").transform.Translate(Vector3.forward * player.GetComponent<PlayerGrap>().grapRange);
    }

    // Update is called once per frame
    void Update()
    {
        CameraRotate();
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y+ 0.6f, player.transform.position.z);
    }

    public void CameraRotate()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");
        rotationX += x * sensitivity * Time.deltaTime;
        rotationY += y * sensitivity * Time.deltaTime;

        if (rotationY > 30)
        {
            rotationY = 30;
        }
        else if (rotationY < -30)
        {
            rotationY = -30;
        }
        transform.eulerAngles = new Vector3(-rotationY, rotationX, 0.0f);
    }
}
