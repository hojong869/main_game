using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGrap : MonoBehaviour
{
    public float grapRange;
    GameObject aimPoint;
    Transform mainCamera;
    RaycastHit hit, Radar;

    bool isMouseDown, searchedBox, isGrapBox;
    int layerMask;

    // Start is called before the first frame update
    void Start()
    {
        aimPoint = GameObject.Find("AimingPoint");
        mainCamera = GameObject.Find("Center").GetComponent<Transform>();
        layerMask = 1 << LayerMask.NameToLayer("Object");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawRay(mainCamera.position , mainCamera.forward * 7, Color.blue, 0.3f);
        if (Physics.Raycast(mainCamera.position, mainCamera.forward, out Radar, grapRange, layerMask))
        {
            if (searchedBox) aimPoint.GetComponent<Image>().color = new Color(255, 0, 0, 1);
            else aimPoint.GetComponent<Image>().color = new Color(255, 0, 0, .3f);
        }
        else
        {
            aimPoint.GetComponent<Image>().color = new Color(0, 0, 0, .5f);
        }

        
        if (Input.GetMouseButtonDown(0))
        {
            isMouseDown = true;
            if (Physics.Raycast(mainCamera.position, mainCamera.forward, out hit, grapRange, layerMask))
            {
                searchedBox = true;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if(searchedBox) unGrap();
            searchedBox = false;
        }
        
        if (searchedBox)
        {
            GrapBox();
        }
    }

    public void GrapBox()
    {
        hit.transform.position = Vector3.Lerp(hit.transform.position, GameObject.Find("GrapPoint").transform.position, 0.1f);
        if (isMouseDown)
        {
            hit.transform.GetComponent<Rigidbody>().isKinematic = true;
            hit.transform.GetComponent<Rigidbody>().useGravity = false;
            //hit.transform.rotation = Quaternion.RotateTowards(hit.transform.rotation, mainCamera.transform.rotation, Time.deltaTime * 1f);
            isMouseDown = false;
            
        }
    }

    public void unGrap() {
        hit.transform.GetComponent<Rigidbody>().isKinematic = false;
        hit.transform.GetComponent<Rigidbody>().useGravity = true;
    }

    public void RotateObject(Vector3 rotate)
    {

    }
}
